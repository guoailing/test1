function setRem(){
    const WIDTH=375;
    let clientWidth=document.documentElement.clientWidth;
    if(clientWidth>=1024){
        clientWidth=1024;
    }
    document.documentElement.style.fontSize=(16*clientWidth)/WIDTH+"px";
}
window.onresize=()=>{
    setRem();
};
setRem();