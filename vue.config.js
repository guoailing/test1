// const path = require("path");
// const px2vw = require("postcss-px-to-viewport");
// module.exports = {
  // css 配置
  //   css: {
  //     loaderOptions: {
  //       postcss: {
  //         plugins: [
  //           require("postcss-pxtorem")({
  //             rootValue: 16,
  //             propList: ["*", "!font-size"],
  //           }),
  //         ],
  //       },
  //     },
  //   },
  //postcss px to viewport

//   css: {
//     loaderOptions: {
//       postcss: {
//         plugins: [
//           new px2vw({
//             unitToConvert: "px",
//             viewportWidth: 375,
//             unitPrecision: 5,
//             propList: ["*"],
//             viewportUnit: "vw",
//             fontViewportUnit: "vw",
//             selectorBlackList: [],
//             minPixelValue: 1,
//             mediaQuery: false,
//             replace: true,
//             exclude: [],
//             landscape: false,
//             landscapeUnit: "vw",
//             landscapeWidth: 568,
//           }),
//         ],
//       },
//     },
//   },
// };


module.exports = {
  // css 配置
  css: {
    loaderOptions: {
      postcss: {
        plugins: [
          require("postcss-pxtorem")({
            rootValue: 100,
            propList: ["*"],
          }),
        ],
      },
    },
  },
};
